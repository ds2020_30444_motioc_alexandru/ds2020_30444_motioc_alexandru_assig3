﻿
using MedicalCareApp.Hubs;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MedicalCareApp.Services
{
    public class Worker : BackgroundService
    {
        private readonly PatientNotificationClass _notifications;

        public Worker(PatientNotificationClass notifications)
        {
            _notifications = notifications;
        }

        public override async Task StartAsync(CancellationToken cancellationToken)
        {
            
            await base.StartAsync(cancellationToken);
        }
     

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                await _notifications.SendToUser("1", "Updated List!");
                await Task.Delay(10000, stoppingToken);
            }
        }
    }
}
