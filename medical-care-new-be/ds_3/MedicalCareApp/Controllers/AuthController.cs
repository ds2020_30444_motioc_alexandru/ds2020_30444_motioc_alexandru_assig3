﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MedicalCareApp.Models;
using MedicalCareApp.Services;
using Microsoft.AspNetCore.Mvc;

namespace MedicalCareApp.Controllers
{
    public class AuthDTO
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class AuthResponse
    {
        public List<string> Roles { get; set; }
        public int UserId { get; set; }
        public string Token { get; internal set; }
    }

    [ApiController]
    [Route("[controller]")]
     public class AuthController : ControllerBase
    {
        private MedicalCareDBContext _context;

        private readonly TokenService _tokenService;
        public AuthController(MedicalCareDBContext context, TokenService tokenService)
        {
            _context = context;
            _tokenService = tokenService;
        }
        //MedicalCareDBEntities db = new MedicalCareDBEntities();
        [HttpGet]
        public string get()
        {
            return "hello";
        }


        [HttpPost]
        // POST api/values
        public AuthResponse Post([FromBody] AuthDTO value)

        {
            if(value.Username == "test")
            {
                var roles = new List<string>();
                roles.Add("patient");
                return new AuthResponse
                {
                    Roles = roles,
                    UserId = 1,
                    Token = _tokenService.CreateToken(1, roles)

                }; 
            }
            User user = _context.Users.Where(u => u.Username == value.Username && u.Password == value.Password).FirstOrDefault();
            if (user != null)
            {
                var roles = new List<string>();
                if (_context.Caregivers.Where(u => u.UserId == user.Id).Any()) 
                {
                    roles.Add("caregiver");
                }
                if (_context.Doctors.Where(u => u.UserId == user.Id).Any())
                {
                    roles.Add("doctor");
                }
                if (_context.Patients.Where(u => u.UserId == user.Id).Any())
                {
                    roles.Add("patient");
                }

                return new AuthResponse
                {
                    Roles = roles,
                    UserId = user.Id,
                    Token = _tokenService.CreateToken(user.Id, roles)

                };
            }
            else
                return null;
        }

      
    }
}
