﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MedicalCareApp.Models;
using Microsoft.EntityFrameworkCore;



namespace MedicalCareApp.Controllers
{

    public class MedicationResponse
    {
        public int auxId { get; set; }

        public string MedicationName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool? MedicationTaken { get; set; }
    }
    [ApiController]
    [Route("[controller]")]
    public class MedicationController : ControllerBase
    {
        private MedicalCareDBContext _context;

        public MedicationController(MedicalCareDBContext context)
        {
            _context = context;
        }


       [HttpGet]
        public IEnumerable<Medication> Get()
        {
            return _context.Medications.ToList();
        }

        [HttpGet("{user_id}")]
        public List<MedicationResponse> Get([FromRoute]int user_id)
        {
            Patient patient =_context.Patients.Where(u => u.UserId == user_id).FirstOrDefault();
            var auxThig = _context.AuxPatientMedications
                .Where(a => a.IdPatient == patient.Id && a.StartDate > DateTime.Now && a.StartDate < (DateTime.Now.AddDays(1)))
                .Include(a => a.IdMedicationNavigation).ToList();
            var response = auxThig.Select(t => new MedicationResponse
            {
                auxId = t.Id,
                MedicationName = t.IdMedicationNavigation.Name,
                StartDate = t.StartDate,
                EndDate = t.EndDate,
                MedicationTaken = t.MedicationTaken

            }).ToList();


            return response;
            
        }

        [HttpPost]
        public void POST(Medication medication)
        {
            _context.Medications.Add(medication);
            _context.SaveChanges();
        }
        
        [HttpPost("take/{id}")]
        public void POST(int id)
        {
           var entry = _context.AuxPatientMedications.FirstOrDefault(a => a.Id == id);
            
            entry.MedicationTaken = true;
            _context.AuxPatientMedications.Update(entry);
            _context.SaveChanges();
        }

       

        [HttpDelete]
        //This method will delete a medication
        public string Delete(int id)
        {
            Medication medication = _context.Medications.Find(id);
            _context.Medications.Remove(medication);
            _context.SaveChanges();
            return "Medication deleted!";
        }
    }
}
