﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MedicalCareApp.Models
{
    public partial class AuxPatientMedication
    {
        public int Id { get; set; }
        public int? IdPatient { get; set; }
        public int? IdMedication { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool? MedicationTaken { get; set; }

        public virtual Medication IdMedicationNavigation { get; set; }
        public virtual Patient IdPatientNavigation { get; set; }
    }
}
