﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MedicalCareApp.Models
{
    public partial class Medication
    {
        public Medication()
        {
            AuxPatientMedications = new HashSet<AuxPatientMedication>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string SideEffects { get; set; }
        public string Dosage { get; set; }

        public virtual ICollection<AuxPatientMedication> AuxPatientMedications { get; set; }
    }
}
