import React, {useState} from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css'
import Pages from './Pages'
import { AuthContext } from "./context/authcontext";
import { ToastProvider, useToasts } from 'react-toast-notifications'

function App() {

  const [medicationList, setMedicationList] = useState([{name: "initial", isTaken: false}, {name: "initial another", isTaken: false}]);
  const [lastupdated, setLastUpdated] = useState("");

  const existingRole = JSON.parse(localStorage.getItem("role"));

  const [role, setRole] = useState(existingRole);


  const setRoleInstorage = (data) => {
    localStorage.setItem("role", JSON.stringify(data));
    setRole(data);
  }

  const existingId = JSON.parse(localStorage.getItem("id"));

  const [id, setId] = useState(existingId);


  const setIdInstorage = (data) => {
    localStorage.setItem("id", JSON.stringify(data));
    setId(data);
  }

  const existingTokens = JSON.parse(localStorage.getItem("tokens"));
  
  const [authTokens, setAuthTokens] = useState(existingTokens);

  const setTokens = (data) => {
    localStorage.setItem("tokens", JSON.stringify(data));
    setAuthTokens(data);
  }

 
 

  return (
    <div className="App">
      <AuthContext.Provider value={{ 
        // adds role and id on authContext
        // can be accessed with useAuth()
        authTokens, setAuthTokens: setTokens, 
        role, setRole: setRoleInstorage ,
        id, setId: setIdInstorage ,
        medicationList, setMedicationList,
        lastupdated, setLastUpdated
      }}>
          <ToastProvider>
     
      <Pages />
      
      </ToastProvider>
      </AuthContext.Provider>
    </div>
  );
}

export default App;
