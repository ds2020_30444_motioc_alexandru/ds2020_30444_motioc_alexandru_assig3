import React, { useState, useEffect} from 'react'
import axios from 'axios';
import { useAuth } from "./../context/authcontext";
import baseUrl from "../url";

export const useDate = () => {
    const locale = 'en';
    const [today, setDate] = React.useState(new Date()); // Save the current date to be able to trigger an update
  
    React.useEffect(() => {
        const timer = setInterval(() => { // Creates an interval which will update the current data every minute
        // This will trigger a rerender every component that uses the useDate hook.
        setDate(new Date());
      }, 60 * 1000);
      return () => {
        clearInterval(timer); // Return a funtion to clear the timer so that it will stop being called on unmount
      }
    }, []);
  
    const day = today.toLocaleDateString(locale, { weekday: 'long' });
    const date = `${day}, ${today.getDate()} ${today.toLocaleDateString(locale, { month: 'long' })}\n\n`;
  
    const hour = today.getHours();
    const wish = `Good ${(hour < 12 && 'Morning') || (hour < 17 && 'Afternoon') || 'Evening'}, `;
  
    const time = today.toLocaleTimeString(locale, { hour: 'numeric', hour12: true, minute: 'numeric' });
  
    return {
      date,
      time,
      wish,
    };
  };



const querystring = require('querystring');
const PatientPage = () => {
    const { id , medicationList, setMedicationList,
      lastupdated, setLastUpdated} = useAuth(); // id of logged in user
    const url = baseUrl + 'Patient/' + id ;
  
    const { date, time, wish } = useDate();
   
    
    return (
        <div className="greetings-container">
        <h1>
            {wish}
            <div >{`${"user " + id || ''}!`}</div>
        </h1>
    
        <div>
            <h3>
            {date}
            <br />
            {time}
            </h3>
        </div>
        <div>Last updated: {lastupdated}</div>
        <div class="list-group">
 
  {medicationList.map(med => {
                    return (
                      <div class="d-flex w-100 justify-content-between">
                        <p class="m-1"> {med.medicationName}</p>
                        {console.log(med)}
                      {!med.medicationTaken && (<button type="button" class="btn btn-primary" onClick={()=>{
                        axios.post(baseUrl+'Medication/take/'+med.auxId)
                       
                      }} >TAKE</button>)}
                      </div>
                    )
                })}
</div>
      
      
        
        </div>
    );
      

}

export default PatientPage