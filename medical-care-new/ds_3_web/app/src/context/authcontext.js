import { createContext, useContext, useEffect, useState, useRef } from 'react';
import * as signalR from '@aspnet/signalr';
import { ToastProvider, useToasts } from 'react-toast-notifications'
import baseUrl from "../url";
import axios from 'axios';


export const AuthContext = createContext();


export function useAuth() {
  
  const context = useContext(AuthContext);
  const { authTokens, setLastUpdated, setMedicationList } = context;
  const { addToast } = useToasts();
  const [isPaused, setPause] = useState(false);
  const ws = useRef(null);
 
  useEffect(() => {
   
    const protocol = new signalR.JsonHubProtocol();
  
    const transport = signalR.HttpTransportType.WebSockets;
  
    const options = {
      transport,
      logMessageContent: true,
      logger: signalR.LogLevel.Error,
      accessTokenFactory: () => authTokens 
    };
  
    // create the connection instance
    ws.current = new signalR.HubConnectionBuilder()
      .withUrl(baseUrl + 'notification', options)
      .withHubProtocol(protocol)
      .build();
     

    ws.current.start()
      .then(() => console.info('SignalR Connected'))
      .catch(err => console.error('SignalR Connection Error: ', err));
  
    return function cleanup() {
      ws.current.stop();
      };
  },[]);

  useEffect(() => {
    
    if (!ws.current) return;

    ws.current.on('notification', (res) => {
      if (isPaused) return;
      axios.get(baseUrl + "/Medication/1").then(json => setMedicationList(json.data))
      //call and set data;
      setLastUpdated(Date.now())
      addToast(res, { appearance: 'success' ,  autoDismiss: true});
    })
  
}, [isPaused]);

  return context;
}